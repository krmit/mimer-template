# Mimer Template

This is a collection of empty project that could be use as a base for 
other project. The chosses made in here is opinited, but it would be 
easy to adopt to youre taste.

## Folders

### src

All source files for the program are located under this folder. They 
should often be organized in subfolders.

### public

A folder where a web server could store static web files for publishing 
on the web.

### test

All unit tests for the program are in this folder. The tests are 
distributed across many files.

### types

Occasionally, you need to write your own types for packages that are 
missing them.

### template

If you have files that will be use as a template for creating other 
files by webserver, you should create this fold.

### dist

The folder contains files generated from the 'src' folder. Usually, it is 
TypeScript files that have been transpiled to JavaScript files. 
Warning! All files here can be overwritten at any time. Do not make 
changes to these files if you do not know what you are doing.

This folder will be created when you build the project for the first 
time.

### doc

The folder contains documentation generatet from 'src' folder. There is 
no reson to changes any file in this folder.

This folder will be created when you generate the documentation for the 
first time.

## Configuration

### For date and times

- **YEAR** When created.
- **YEARS** When created or span of development in years. Example:  1997-2022
- **DATE** Relevant date. Example: 2022-02-22  

### For each project

- **GIT_PATH** The domain and path to git arcive.
- **ISSUES_URL** URL to the issues handler.
- **PROJECT** Name of projekt.
- **COMMAND** Name of command runing project from terminal.
- **LICENSE** Name of the licens, see below.

### For a developer

- **AUTHOR**: Your name.
- **MY_URL**: URL to my homepage.

## Licence

### GPLv3

- GNU GENERAL PUBLIC LICENSE Version 3
- "license": "GPL-3.0-or-later",
- gpl-3.0.txt

### AGPLv3

- GNU AFFERO GENERAL PUBLIC LICENSE Version 3
- "license": "AGPL-3.0-or-later",
- agpl-3.0.txt

### ISC

- Internet Systems Consortium
- "license": "ICS",
- icl.txt
- Ypu must replace variabel YEARS and AUTHOR in license text. 
