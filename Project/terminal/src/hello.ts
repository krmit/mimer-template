import prompt from "promise-prompt";
import colors from "colors";

export async function hello() {
  const name = prompt("What is your name? ");
  console.log(creatHelloText(await name));
}

export function creatHelloText(msg: string) {
  return "Hello " + colors.bold.red(msg) + "!";
}
