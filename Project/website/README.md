# NAME

## Install

If you would like to use the development toll for the site.

```
$ npm install
```

## Usage

To test a site on local computer with live update you could start a web server.

```
npm run start
```

To use opinionated code formatter to get nice format on your code.

```
npm run fmt
```

## Changelog

- **0.0.1** _{{year}}-??-??_ First version published

## Author

**©AUTHOR YEAR [MY_URL](MY_URL)**
