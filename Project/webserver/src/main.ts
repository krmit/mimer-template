#!/usr/bin/env node
import express from "express";
import { logger } from "./logger.js";
import { notFound, serverError } from "./errors.js";
export { hello } from "./hello.js";

async function main() {
  const app = express();

  app.use(logger);

  // If you need to parse url.
  //app.use(express.urlencoded({ extended: true }));

  app.get("/hello/:world", function (req, res) {
    res.send("Hello " + req.params.world + "!");
  });

  app.get("/error", function (req, res) {
    throw "Test throwing Error";
  });

  app.use("/", express.static("public"));

  app.use(notFound);

  app.use(serverError);

  const port = 8080;

  app.listen(port, () => {
    console.log("You can find this server on: http://localhost:" + port);
  });
}

main();
